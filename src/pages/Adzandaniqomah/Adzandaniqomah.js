import React from 'react';
import { Text, View,Image } from 'react-native';

const Adzandaniqomah = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4", marginTop:50,alignContent:'center',alignItems:'center' }} >
             <Text style={{color:"#FFFFFF",fontSize:16}}>1. Lafadz Adzan</Text>
             <Text style={{color:"#FFFFFF"}}>2x اَللهُ اَكْبَرُ،اَللهُ اَكْبَرُ</Text>
             <Text style={{color:"#FFFFFF"}}>2x أَشْهَدُ اَنْ لاَ إِلٰهَ إِلَّااللهُ</Text>
             <Text style={{color:"#FFFFFF"}}>2x اَشْهَدُ اَنَّ مُحَمَّدًا رَسُوْلُ اللهِ</Text>
             <Text style={{color:"#FFFFFF"}}>2x حَيَّ عَلَى الصَّلاَةِ</Text>
             <Text style={{color:"#FFFFFF"}}>2x حَيَّ عَلَى الْفَلاَحِ</Text>
             <Text style={{color:"#FFFFFF"}}>2x اَللهُ اَكْبَرُ،اَللهُ اَكْبَرُ</Text>
             <Text style={{color:"#FFFFFF"}}>1x لَا إِلَهَ إِلَّااللهُ</Text> 


             <Text style={{color:"#FFFFFF",fontSize:16}}>2. Lafadz Iqomah</Text>
             <Text style={{color:"#FFFFFF"}}>للهُ اَكْبَرُ ،اَللهُ اَكْبَرُ</Text>
             <Text style={{color:"#FFFFFF"}}>أَشْهَدُ اَنْ لَا إِلٰهَ إِلَّااللهُ</Text>
             <Text style={{color:"#FFFFFF"}}>اَشْهَدُ اَنَّ مُحَمَّدًا رَسُوْلُ اللهِ</Text>
             <Text style={{color:"#FFFFFF"}}>حَيَّ عَلَى الصَّلَاةِ</Text>
             <Text style={{color:"#FFFFFF"}}>حَيَّ عَلَى الْفَلَاحِ</Text>
             <Text style={{color:"#FFFFFF"}}>قَدْ قَامَتِ الصَّلَاةُ ،قَدْ قَامَتِ الصَّلَاةُ</Text>
             <Text style={{color:"#FFFFFF"}}>اَللهُ اَكْبَرُ ،اَللهُ اَكْبَرُ</Text>
             <Text style={{color:"#FFFFFF"}}>لَاإِلٰهَ إِلاَّاللهُ</Text>

             <Text style={{color:"#FFFFFF",fontSize:16}}>3. Do'a Setelah Adzan</Text>
             <Text style={{color:"#FFFFFF"}}>اَللّٰهُمَّ رَبَّ هٰذِهِ الدَّعْوَةِ التَّآمَّةِ، وَالصَّلاَةِ الْقَآئِمَةِ،</Text>
             <Text style={{color:"#FFFFFF"}}>  آتِ مُحَمَّدَانِ الْوَسِيْلَةَ وَالْفَضِيْلَةَ وَالشَّرَفَ</Text>
             <Text style={{color:"#FFFFFF"}}>   وَالدَّرَجَةَ الْعَالِيَةَ الرَّفِيْعَةَ وَابْعَثْهُ مَقَامًامَحْمُوْدَانِ</Text>
             <Text style={{color:"#FFFFFF"}}>  الَّذِىْ وَعَدْتَهُ اِنَّكَ لاَتُخْلِفُ الْمِيْعَادَ يَآاَرْحَمَ الرَّحِمِيْنَ</Text>
                
            <Text style={{color:"#FFFFFF",fontSize:16}}>4. Do'a Setelah Iqomah</Text>
            <Text style={{color:"#FFFFFF"}}>اَقَامَهَااللهُ وَاَدَامَهَا مَادَامَتِ السَّمَوَاتُ وَاْلاَرْضُ</Text>
   
        </View>
        
    )
}

export default Adzandaniqomah;