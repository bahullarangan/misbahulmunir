import React from 'react';
import { Text, View,Image } from 'react-native';

const Bilalhariraya = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4", marginTop:50,alignContent:'center',alignItems:'center' }} >
            <Text style={{color:"#FFFFFF",fontSize:16}}>1. Idhul Adha</Text>
             <Text style={{color:"#FFFFFF"}} >يَا مَعَاشِرَ اْلـمُسْلِمِيْنَ وَزُمْرَةَ اْلـمُؤْمِنِيْنَ رَحِمَكُمُ اللهُ</Text>
             <Text style={{color:"#FFFFFF"}}> إِعلَمُوا أَنَّ يَوْمَكُمْ هَذَا يَوْمُ عِيْدِ الأَضْحَى وَيَومُ السُرُوْرِ</Text>
             <Text style={{color:"#FFFFFF"}}>  وَيَومُ المــــــغْفُورِ يَومُ اَحَلَّ اللهُ</Text>
                  <Text style={{color:"#FFFFFF"}}> لَكُمْ فِيهِ الطَعَامَ وَحَرَّمَ عَلَيْكُمْ الصِّيَامَ إِذَا صَعِدَ</Text>
                   <Text style={{color:"#FFFFFF"}}>  الخَطِيبُ عَلَى المِنْبَرِ أَنْصِتُوْا وَاسْمَعُوْا وَأَطِيْعُوْا رَحِمَكُمُ اللهُ</Text>
                   <Text style={{color:"#FFFFFF"}}> , أَنْصِتُوْا وَاسْمَعُوْا وَأَطِيْعُوْا</Text>
                  <Text style={{color:"#FFFFFF"}}>  أجرَكُمُ اللهُ,  أَنْصِتُوْا وَاسْمَعُوْا وَأَطِيْعُوْا لَعَلَّكُمْ تُرْحَمُوْنَ</Text>
             <Text style={{color:"#FFFFFF"}} >Kemudian Khotib naik ke mimbar, bilal </Text>
             <Text style={{color:"#FFFFFF"}}>  memperlihatkan tongkat dan membaca sholawat : </Text>
             <Text style={{color:"#FFFFFF"}}>اَللّهُمَّ صَلِّ عَلَى سَيِّدِنَا مُحَمَّدٍ. اَللّهُمَّ صَلِّ وَسَلِّمْ عَلَى سَيِّدِنَا مُحَمَّدٍ.</Text>
             <Text style={{color:"#FFFFFF"}}> اَللّهُمَّ صَلِّ وَسَلِّمْ عَلَى سَيِّدِنَا مُحَمَّدٍ وَعَلَى آلِهِ وَصَحْبِهِ اَجْمَعِيْنَ</Text>
             
             <Text style={{color:"#FFFFFF"}} >Lalu bilal menghadap qiblat </Text>
             <Text style={{color:"#FFFFFF"}} >dan membaca doa sebagai berikut:</Text>

            <Text style={{color:"#FFFFFF"}}>اَللهُمَّ قَوِّاْلاِسْلاَمِ مِنَ الْمُسْلِمِيْنَ وَالْمُسْلِمَاتِ، وَالْمُؤْمِنِيْنَ وَالْمُؤْمِنَاتِ،</Text>
            <Text style={{color:"#FFFFFF"}}>  اْلاَحْيَاءِ مِنْهُمْ وَاْلأَمْوَاتِ، وَيَسِّرْهُمْ عَلَى مَعَانِدِ الدِّيْن،</Text>
                 <Text style={{color:"#FFFFFF"}}> وَاخْتِمْ لَنَا مِنْكَ بِالْخَيْرِ، وَيَاخَيْرَالنَّاصِرَيْنَ بِرَحْمَتِكَ يَااَرْحَمَ الرَّاحِمِيْنَ</Text>
            

            <Text style={{color:"#FFFFFF",fontSize:16}}>2. Idhul Fitrih</Text>
             <Text style={{color:"#FFFFFF"}} >يَا مَعَاشِرَ اْلـمُسْلِمِيْنَ وَزُمْرَةَ اْلـمُؤْمِنِيْنَ رَحِمَكُمُ اللهُ إِعلَمُوا </Text>
             <Text style={{color:"#FFFFFF"}}> أَنَّ يَوْمَكُمْ هَذَا يَوْمُ عِيْدِ الْفِطْرِ وَيَومُ السُرُوْرِ</Text>
                 <Text style={{color:"#FFFFFF"}}> وَيَومُ المــــــغْفُورِ يَومُ اَحَلَّ اللهُ لَكُمْ فِيهِ الطَعَامَ وَحَرَّمَ عَلَيْكُمْ الصِّيَامَ </Text>
                  <Text style={{color:"#FFFFFF"}}> إِذَا صَعِدَ الخَطِيبُ عَلَى المِنْبَرِ</Text>
                  <Text style={{color:"#FFFFFF"}}> أَنْصِتُوْا وَاسْمَعُوْا وَأَطِيْعُوْا رَحِمَكُمُ اللهُ</Text>
                  <Text style={{color:"#FFFFFF"}}>, أَنْصِتُوْا وَاسْمَعُوْا وَأَطِيْعُوْا أجرَكُمُ اللهُ, </Text>
                  <Text style={{color:"#FFFFFF"}}> أَنْصِتُوْا وَاسْمَعُوْا وَأَطِيْعُوْا لَعَلَّكُمْ تُرْحَمُوْنَ</Text>
             <Text style={{color:"#FFFFFF"}} >Kemudian Khotib naik ke mimbar, </Text>
             <Text style={{color:"#FFFFFF"}}> bilal memperlihatkan tongkat dan membaca sholawat : </Text>
             <Text style={{color:"#FFFFFF"}}>اَللّهُمَّ صَلِّ عَلَى سَيِّدِنَا مُحَمَّدٍ. اَللّهُمَّ صَلِّ وَسَلِّمْ عَلَى سَيِّدِنَا مُحَمَّدٍ.</Text>
             <Text style={{color:"#FFFFFF"}}> اَللّهُمَّ صَلِّ وَسَلِّمْ عَلَى سَيِّدِنَا مُحَمَّدٍ وَعَلَى آلِهِ وَصَحْبِهِ اَجْمَعِيْنَ</Text>
             
             <Text style={{color:"#FFFFFF"}} >Lalu bilal menghadap qiblat </Text>
             <Text style={{color:"#FFFFFF"}}> dan membaca doa sebagai berikut:</Text>

            <Text style={{color:"#FFFFFF"}}>اَللهُمَّ قَوِّاْلاِسْلاَمِ مِنَ الْمُسْلِمِيْنَ وَالْمُسْلِمَاتِ، وَالْمُؤْمِنِيْنَ وَالْمُؤْمِنَاتِ،</Text>
            <Text style={{color:"#FFFFFF"}}> اْلاَحْيَاءِ مِنْهُمْ وَاْلأَمْوَاتِ، وَيَسِّرْهُمْ عَلَى مَعَانِدِ الدِّيْن،</Text>
                 <Text style={{color:"#FFFFFF"}}> وَاخْتِمْ لَنَا مِنْكَ بِالْخَيْرِ، وَيَاخَيْرَالنَّاصِرَيْنَ بِرَحْمَتِكَ يَااَرْحَمَ الرَّاحِمِيْنَ</Text>
        </View>
    )
}

export default Bilalhariraya;