import React, {useEffect} from 'react';
import { Component } from 'react';
import { View, Text,Image,StatusBar,} from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';


class Detail extends Component{

    constructor(props) {
        super(props);
      
    }

    render() {
     
     
  
      return(
        <View  style={{flex: 1,backgroundColor:"#03a9f4" }}>
             
          <Text style={{textAlign: 'center', marginTop: 5,  fontSize: 20,color:"#FFFFFF"}}>{this.props.route.params.judul}</Text>
          <Text style={{textAlign: 'center', marginTop: 5,  fontSize: 20,color:"#FFFFFF"}}>{this.props.route.params.keterangan}</Text>
          <ScrollView>
          <Text style={{textAlign: 'center', marginTop: 5,  fontSize: 16,color:"#FFFFFF"}}>{this.props.route.params.isi}</Text>
          </ScrollView>
    </View>

      );
    }
  }
  export default Detail;