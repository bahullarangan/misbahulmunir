import  React, {Component, component}  from "react";
import { StatusBar, View,Text,TextInput,FlatList, TouchableOpacity } from "react-native";
import { Hoshi } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/FontAwesome5'
import Moqoddimah from "../Moqoddimah/indek";

import Sholatfarduh from "../Sholatfarduh/Sholatfarduh"
import Sholatduha from "../Sholatduha/Sholatduha"
import Sholattahajjut from "../Sholattahajjut/Sholattahujjut"
import Sholatjanazah from "../Sholatjanazah/Sholatjanazah"
import Bilalhariraya from "../Bilalhariraya/Bilalhariraya"
import Adzandaniqomah from "../Adzandaniqomah/Adzandaniqomah"
import Sholawat1 from "../Sholawat1/Sholawat1"
import Sholawat2 from "../Sholawat2/Sholawat2"
import Sholawat3 from "../Sholawat3/Sholawat3"
import Syiir from "../Syiir/Syiir"


let database =[
  {id :0,judul: 'Moqoddimah',keterangan: 'Pendahuluan',isi:<Moqoddimah/>},
  {id :2,judul: 'Tata Cara',keterangan: "Sholat Lima Waktu",isi:<Sholatfarduh/>},
  {id :3,judul: "Sholat Dhuha ",keterangan:"Dan Do'a Sholat Dhuha",isi:<Sholatduha/>},
  {id :4,judul: "Sholat Tahajjut ",keterangan:"Dan Do'a Sholat Tahajjut",isi:<Sholattahajjut/>},
  {id :5,judul: "Sholat Janazah ",keterangan: "Dan Do'a Sholat Janazah",isi:<Sholatjanazah/>},
  {id :6,judul: 'Bilal Hari Raya ',keterangan: 'Idul Fitrih dan Idul Adha',isi:<Bilalhariraya/>},
  {id :7,judul: "Adzan Dan Iqomah",keterangan: "Do'a Adzan Dan Iqomah",isi:<Adzandaniqomah/>},
  {id :8,judul: 'Sholawat1',keterangan: 'Nariya',isi:<Sholawat1/>},
  {id :9,judul: 'Sholawat2',keterangan: 'Syifak',isi:<Sholawat2/>},
  {id :10,judul: 'Sholawat3',keterangan: 'Fatih',isi:<Sholawat3/>},
  {id :11,judul: "Syi'ir ",keterangan: 'Abu Nawas',isi:<Syiir/>}
  
]

class Home extends Component {
  constructor(props) {
    super (props);
    this.state ={
      text: '',
      data: database
     
    };
  }
  search = () => {
    let data= database

    data = data.filter(item => item.judul.toLocaleLowerCase().includes(this.state.text.toLocaleLowerCase()));

    this.setState({
      data: data
    })
  }
  render(){
    return(
        <View style={{flex:1}}>
          <StatusBar backgroundColor="#0288d1" barStyle="light-content"/>
          <View style={{padding: 20, backgroundColor: "#03a9f4",elevation: 1,flexDirection: 'row',aligmnItems: 'center'}}>
          <View style={{flex:1}}>
          <Text style={{textAlign: 'center',color:'#FFFFFF', fontweight: 'bold', fontSize: 18}}>Daftar Isi</Text>
          </View>
            
          </View>

          <View style={{marginHorizontal:20, marginVertical:10,backgroundColor:'#F9F7F6'}}>
          <Hoshi
          label={'Masukkan kata kunci'}
          borderColor={'#03a9f4'}
          borderHight={3}
          inputPadding={16}
          backgroundColor={'#F9F7F6'}

          onChangeText={text => this.setState({text: text})}
          value={this.state.text}
          onKeyPress={() => this.search()}

          />
          </View>
            <FlatList
        data={this.state.data}
        renderItem={({item}) =>
        <TouchableOpacity style={{borderWidth :1,borderRadius: 3,marginVertical: 5,marginHorizontal:20,padding: 10,flexDirection:'row',alignItems:'center'}}
        onPress={() => this.props.navigation.navigate('Detail', {judul:item.judul, keterangan:item.keterangan, isi: item.isi})}>

        <View style={{flex: 1}}>
          <Text style={{fontSize:18, fontWeight:'bold'}} >{item.judul}</Text>
          <Text style={{fontSize:16,marginTop:5}}>{item.keterangan}</Text>
        </View>
          <Icon name="chevron-right" size={25} color="#03a9f4" style={{marginRight: 10}}/>
        </TouchableOpacity>
        }
        keyExtractor={item => item.judul}
      />
          
          
          
          </View>

    );
  }
}
export default Home;