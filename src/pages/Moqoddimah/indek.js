import React from 'react';
import { Text, View,Image,FlatList } from 'react-native';

const Moqoddimah = () => {
    return(
        
        <View style={{flex: 1,backgroundColor:"#03a9f4", marginTop:50,alignContent:'center',alignItems:'center' }} >
            
             <Text style={{color:"#FFFFFF"}} >بسم الله الرحمن الحيم</Text>
             <Text style={{color:"#FFFFFF"}} >الحمد لله الذي يهدي من يشاء ال الصراط المستقيم</Text>
             <Text style={{color:"#FFFFFF"}} > والصلاة والسلام على العاشق لمناجة ربه سيدنا محمد وعلى اله واصحابه </Text>
             <Text style={{color:"#FFFFFF"}} > المجاهدين مدة ذكر الذاكرين وسهو الغافلين اما بعد</Text>
             <Text style={{color:"#FFFFFF"}} >Kepada semua pembaca yang budiman,</Text> 
             <Text style={{color:"#FFFFFF"}}>kami dengan ikhlas</Text>
             <Text style={{color:"#FFFFFF"}}> mengumpulkan kitab ini dengan </Text>
             <Text style={{color:"#FFFFFF"}} >harapan semoga allah mempermudah </Text>
             <Text style={{color:"#FFFFFF"}}>terhadap orang yang berkeinginan</Text>
             <Text style={{color:"#FFFFFF"}}> mengamalkan kitab ini </Text>
             <Text style={{color:"#FFFFFF"}} > dan semoga menjadi kemanfaatan besar</Text>
             <Text style={{color:"#FFFFFF"}}> kepada kita semua. Amiin.</Text>
            <Text style={{color:"#FFFFFF"}}>Dan harapan dari kami bila mana terdapat kesalahan</Text>
            <Text style={{color:"#FFFFFF"}}> maupun kekeliruan  baik dari tulisan, </Text>
            <Text style={{color:"#FFFFFF"}}> atau susunan kami mengharap untuk diperbaiki,</Text>
                
            <Text style={{color:"#FFFFFF"}}> Sekian terimakasih.</Text>
            <Text style={{color:"#FFFFFF"}}>استغفرالله رب البرايا     استغفرالله من الخطايا</Text>
            <Text style={{color:"#FFFFFF"}}> ربي زدني علما نافعا           ووفقني عملا مقبولا</Text>
           
        </View>
         
    )
}

export default Moqoddimah;