import React from 'react';
import { Text, View,Image } from 'react-native';

const Sholatduha = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4",alignContent:'center',alignItems:'center' }} >
             <Text style={{color:"#FFFFFF"}} >اُصَلِّى سُنَّةَ الضَّحٰى رَكْعَتَيْنِ مُسْتَقْبِلَ الْقِبْلَةِ اَدَاءً ِللهِ تَعَالَى</Text>
             <Text style={{color:"#FFFFFF"}}>Pada Rokaat Pertama Setelah Fatihah </Text>
             <Text style={{color:"#FFFFFF"}}>  Disunnahkan Membaca Surah Al-kafirun </Text>
             <Text style={{color:"#FFFFFF"}}>Pada Rokaat Kedua Setelah Fatihah</Text>
             <Text style={{color:"#FFFFFF"}}>Disunnahkan Membaca Surah Al-Ikhlas </Text>
            <Text style={{color:"#FFFFFF"}}> Setelah Sholat Membaca Do'a berikut :</Text>

            <Text style={{color:"#FFFFFF"}}>اَللّٰهُمَّ اِنَّ الضُّحَآءَ ضُحَاءُكَ وَالْبَهَاءَ بَهَاءُكَ وَالْجَمَالَ جَمَالُكَ </Text>
               <Text style={{color:"#FFFFFF"}}> وَالْقُوَّةَ قُوَّتُكَ وَالْقُدْرَةَ قُدْرَتُكَ وَالْعِصْمَةَ عِصْمَتُكَ </Text>  
               <Text style={{color:"#FFFFFF"}}>اَللّٰهُمَّ اِنْ كَانَ رِزْقِى فِى السَّمَآءِ فَأَنْزِلْهُ وَاِنْ كَانَ فِى اْلاَرْضِ فَأَخْرِجْهُ </Text>
               <Text style={{color:"#FFFFFF"}}>وَاِنْ كَانَ مُعَسَّرًا فَيَسِّرْهُ وَاِنْ كَانَ حَرَامًا فَطَهِّرْهُ </Text>
               <Text style={{color:"#FFFFFF"}}> وَاِنْ كَانَ بَعِيْدًا فَقَرِّبْهُ بِحَقِّ ضُحَاءِكَ وَبَهَاءِكَ وَجَمَالِكَ </Text>
               <Text style={{color:"#FFFFFF"}}>وَقُوَّتِكَ وَقُدْرَتِكَ آتِنِىْ مَآاَتَيْتَ عِبَادَكَ الصَّالِحِيْنَ
            </Text>
        </View>
    )
}

export default Sholatduha;