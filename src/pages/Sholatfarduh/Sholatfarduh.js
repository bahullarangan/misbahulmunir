import React from 'react';
import { Text, View,Image } from 'react-native';

const Sholatfarduh = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4",alignContent:'center',alignItems:'center',marginTop:20 }} >
            <Text style={{fontSize:16,color:"#FFFFFF"}}>1. Niat Sholat</Text>
             <Text style={{color:"#FFFFFF"}}>Sholat Subuh</Text>
             <Text style={{color:"#FFFFFF"}} >أصلي فرض الصبح ركعتين مستقبل القبلة</Text >
             <Text style={{color:"#FFFFFF"}} >أداء/مأموما/إماما لله تعالى</Text>
             <Text style={{color:"#FFFFFF"}} >Sholat Dhuhur</Text>
             <Text style={{color:"#FFFFFF"}} >أصلي فرض الظهر أربع ركعات مستقبل القبلة</Text>
             <Text style={{color:"#FFFFFF"}} > أداء/مأموما/إماما لله تعالى</Text>
             <Text style={{color:"#FFFFFF"}} >Sholat Ashar</Text>
             <Text style={{color:"#FFFFFF"}} >أصلي فرض العصر أربع ركعات مستقبل القبلة</Text>
             <Text style={{color:"#FFFFFF"}}> أداء/مأموما/إماما لله تعالى</Text>
             <Text style={{color:"#FFFFFF"}} >Sholat Maghrib</Text>
             <Text style={{color:"#FFFFFF"}} >أصلي فرض المغرب ثلاث ركعات مستقبل القبلة</Text>
             <Text style={{color:"#FFFFFF"}}> أداء/مأموما/إماما لله تعالى</Text>
             <Text style={{color:"#FFFFFF"}} >Sholat Isyak</Text>
             <Text style={{color:"#FFFFFF"}} >أصلي فرض العشاء أربع ركعات مستقبل القبلة </Text>
             <Text style={{color:"#FFFFFF"}}>أداء/مأموما/إماما لله تعالى</Text>
             
             <Text style={{color:"#FFFFFF",fontSize:16}} >2. Do'a Iftitah</Text> 
             <Text style={{color:"#FFFFFF"}}>كَبِيرًا، وَالْحَمْدُ لِلَّهِ كَثِيرًا، وَسُبْحَانَ اللهِ بُكْرَةً وَأَصِيلًا </Text>
             <Text style={{color:"#FFFFFF"}}> وَجَّهْتُ وَجْهِيَ لِلَّذِيْ فَطَر السَّمَوَاتِ وَالْأَرْضَ</Text>
             <Text style={{color:"#FFFFFF"}}> حَنِيْفاً مُسْلِماً وَمَا أَنَا مِنَ الْمُشْرِكِيْنَ.  </Text>
             <Text style={{color:"#FFFFFF"}}> إِنَّ صَلَاتِيْ وَنُسُكِيْ وَمَحْيَايَ وَمَمَاتِيْ لِلَّهِ رَبِّ الْعَالَمِيْنَ </Text>
             <Text style={{color:"#FFFFFF"}}> لَا شَرِيْكَ لَهُ وَبِذَلِكَ أُمِرْتُ وَأَنَا مِنَ الْمُسْلِمِيْنَ
             </Text>
             <Text style={{color:"#FFFFFF",fontSize:16}}>3. Membaca Alfatihah</Text>
             <Text style={{color:"#FFFFFF"}}>بِسْمِ اللّهِ الرَّحْمَنِ الرَّحِيْمِ .</Text>
             <Text style={{color:"#FFFFFF"}}> الْحَمْدُ لِلَّهِ رَبِّ الْعَالَمِينَ . الرَّحْمَٰنِ الرَّحِيم </Text>
             <Text style={{color:"#FFFFFF"}}>. مَالِكِ يَوْمِ الدِّينِ . إِيَّاكَ نَعْبُدُ وَإِيَّاكَ نَسْتَعِينُ .</Text>
             <Text style={{color:"#FFFFFF"}}>اهْدِنَا الصِّرَاطَ الْمُسْتَقِيمَ .</Text>
             <Text style={{color:"#FFFFFF"}}>صِرَاطَ الَّذِينَ أَنْعَمْتَ عَلَيْهِمْ غَيْرِ الْمَغْضُوبِ عَلَيْهِمْ وَلَا الضَّالِّينَ</Text>
              <Text style={{color:"#FFFFFF"}}>Diteruskan Membaca Surah2 Pendek</Text>
              <Text style={{color:"#FFFFFF",fontSize:16}}>4. Rukuk</Text>  
              <Text style={{color:"#FFFFFF"}}> 3x سبحان ربي العظيم وبحمده</Text> 
              <Text style={{color:"#FFFFFF",fontSize:16}}>5. I'tidal</Text>
              <Text style={{color:"#FFFFFF"}}>سمع الله لمن حمده </Text>
              <Text style={{color:"#FFFFFF"}}>ربنا لك اللحمد ملء السموات وملء الأرض وملء ما شئت من شيء بعد
              </Text>
              <Text style={{color:"#FFFFFF",fontSize:16}}>6. Sujud</Text>
              <Text style={{color:"#FFFFFF"}}>3x سبحان ربي الأعلى وبحمده </Text>
              <Text style={{color:"#FFFFFF",fontSize:16}}>7. Duduk Diantara Dua sujud</Text>
              <Text style={{color:"#FFFFFF"}}>رب اغفررلي وارحمني واجبرني وارفعني </Text>
              <Text style={{color:"#FFFFFF"}}> وارزقني واههدني وعافني واعف عني</Text>
              <Text style={{color:"#FFFFFF",fontSize:16}}>8. Tasyahud awal</Text>
              <Text style={{color:"#FFFFFF"}}>التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ لِلَّهِ </Text>
              <Text style={{color:"#FFFFFF"}}> السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِىُّ وَرَحْمَةُ اللَّهِ وَبَرَكَاتُهُ </Text>
              <Text style={{color:"#FFFFFF"}}>السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللَّهِ الصَّالِحِينَ أَشْهَدُ </Text>
              <Text style={{color:"#FFFFFF"}}>أَنْ لاَ إِلَهَ إِلاَّ اللَّهُ وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُولُ اللَّهِاَ . للَّهُمَّ صَلِّ عَلىَ مُحَمَّدٍ</Text>
                <Text style={{color:"#FFFFFF",fontSize:16}}>9. Tasyahud Akhir</Text>
                <Text style={{color:"#FFFFFF"}}>اَللَّهُمَّ صَلِّ عَلىَ مُحَمَّدٍ وَعَلىَ آلِ مُحَمَّدٍ</Text>
                 <Text style={{color:"#FFFFFF"}}>كَماَ صَلَّيْتَ عَلىَ إِبْرَاهِيْمَ وَعَلىَ آلِ إِبْرَاهِيْمَ</Text>
                 <Text style={{color:"#FFFFFF"}}> إِنـَّكَ حَمِيْدٌ مَجِيْدٌ اَللَّهُمَّ باَرِكْ عَلىَ مُحَمَّدٍ وَعَلىَ </Text>
                 <Text style={{color:"#FFFFFF"}}> آلِ مُحَمَّدٍ كَماَ باَرَكْتَ عَلىَ إِبْرَاهِيْمَ وَعَلىَ آلِ إِبْرَاهِيْمَ إِنـَّكَ حَمِيْدٌ مَجِيْدٌ</Text>
             <Text style={{color:"#FFFFFF",fontSize:16}}>10. Salam</Text>
             <Text style={{color:"#FFFFFF"}}>السَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ اللهِ</Text>
        </View>
    )
}

export default Sholatfarduh;