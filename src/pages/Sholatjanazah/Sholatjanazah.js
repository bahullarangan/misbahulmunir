import React from 'react';
import { Text, View,Image } from 'react-native';

const Sholatjanazah = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4",alignContent:'center',alignItems:'center',marginTop:20 }} >
            <Text style={{color:"#FFFFFF"}}>Takbir Pertama Niat Dan Membaca Alfatihah</Text>
             <Text style={{color:"#FFFFFF"}} >Janazah Laki-Laki</Text>
             <Text style={{color:"#FFFFFF"}} >أُصَلِّيْ عَلَى هَذَا الْمَيِّتِ /مأموما/إماما فَرْضًا لِلهِ تَعَالَى</Text>
             <Text style={{color:"#FFFFFF"}} >Janazah Perempuan</Text>
             <Text style={{color:"#FFFFFF"}} >أُصَلِّى عَلَى هَذِهِ الْمَيِّتَةِ /مأموما/إماما فَرْضًا لِلهِ تَعَالَى</Text>

             <Text style={{color:"#FFFFFF"}}>Takbir Kedua Membaca Sholawat</Text>
             <Text style={{color:"#FFFFFF"}}>اَللَّهُمَّ صَلِّ عَلىَ مُحَمَّدٍ وَعَلىَ آلِ مُحَمَّدٍ كَماَ صَلَّيْتَ عَلىَ إِبْرَاهِيْمَ</Text>
             <Text style={{color:"#FFFFFF"}}>  وَعَلىَ آلِ إِبْرَاهِيْمَ إِنـَّكَ حَمِيْدٌ مَجِيْدٌ اَللَّهُمَّ باَرِكْ عَلىَ مُحَمَّدٍ  </Text>
             <Text style={{color:"#FFFFFF"}}>وَعَلىَ آلِ مُحَمَّدٍ كَماَ باَرَكْتَ عَلىَ إِبْرَاهِيْمَ</Text>
             <Text style={{color:"#FFFFFF"}}> وَعَلىَ آلِ إِبْرَاهِيْمَ إِنـَّكَ حَمِيْدٌ مَجِيْدٌ</Text>
            <Text style={{color:"#FFFFFF"}}>Takbir Ketiga Membaca Do'a</Text>
            <Text style={{color:"#FFFFFF"}}>اللَّهُمَّ اغْفِرْ لَهُ وَارْحَمْهُ وَعَافِهِ وَاعْفُ عَنْهُ وَأَكْرِمْ نُزُلَهُ وَوَسِّعْ مُدْخَلَهُ</Text>
            <Text style={{color:"#FFFFFF"}}> وَاغْسِلْهُ بِالْمَاءِ وَالثَّلْجِ وَالْبَرَدِ وَنَقِّهِ مِنَ الْخَطَايَا كَمَا نَقَّيْتَ </Text>
            <Text style={{color:"#FFFFFF"}}>بْيَضَ مِنَ الدَّنَسِ وَأَبْدِلْهُ دَارًا خَيْرًا مِنْ دَارِهِ</Text>
            <Text style={{color:"#FFFFFF"}}> وَأَهْلاً خَيْرًا مِنْ أَهْلِهِ وَزَوْجًا خَيْرًا مِنْ زَوْجِهِ</Text>
            <Text style={{color:"#FFFFFF"}}>  وَأَدْخِلْهُ الْجَنَّةَ وَأَعِذْهُ مِنْ عَذَابِ الْقَبْرِ أَوْ مِنْ عَذَابِ النَّارِ</Text>
           <Text style={{color:"#FFFFFF"}}>Takbir Keempat Membaca Do'a</Text>
           <Text style={{color:"#FFFFFF"}}>اللَّهُمَّ لاَ تَحْرِمْنَا أَجْرَهُ وَلاَ تَفْتِنَّا بَعْدَهُ وَ اغْفِرْ لَنَا وَلَهُ</Text> 
           <Text style={{color:"#FFFFFF"}}>Membaca Salam</Text>
           <Text style={{color:"red"}}>Catatan Bila Janazah Laki-laki, </Text>
           <Text style={{color:"red"}}> Maka Dhomir Yang Dipakai Adalah Laki-laki (ه)</Text>
           <Text style={{color:"red"}}>Catatan Bila Janazah Perempuan, </Text>
           <Text style={{color:"red"}}> Maka Dhomir Yang Dipakai Adalah Perempuan (ها)</Text>
        </View>
    )
}

export default Sholatjanazah;