import React from 'react';
import { Text, View,Image } from 'react-native';

const Sholattahajjut = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4",alignContent:'center',alignItems:'center',marginTop:20 }} >
             <Text style={{color:"#FFFFFF"}} >اُصَلِّى سُنَّةً التَّهَجُّدِ رَكْعَتَيْنِ مُسْتَقْبِلَ الْقِبْلَةِ ِللهِ تَعَالَى</Text>
             <Text style={{color:"#FFFFFF"}} >Berikut Do'anya</Text>
             <Text style={{color:"#FFFFFF"}}> اَللهُمَّ رَبَّنَا لَكَ الْحَمْدُ اَنْتَ قَيِّمُ السَّمَوَاتِ وَاْلاَرْضِ وَمَنْ فِيْهِنَّ</Text>
                <Text style={{color:"#FFFFFF"}}> .وَلَكَ الْحَمْدُ اَنْتَ مَلِكُ السَّمَوَاتِ واْلاَرْضِ وَمَنْ فِيْهِنَّ. </Text>
                <Text style={{color:"#FFFFFF"}}> وَلَكَ الْحَمْدُ اَنْتَ نُوْرُ السَّمَوَاتِ وَاْلاَرْضِ وَمَنْ فِيْهِنَّ. </Text>
                <Text style={{color:"#FFFFFF"}}>وَلَكَ الْحَمْدُ اَنْتَ الْحَقُّ وَوَعْدُكَ الْحَقُّ وَلِقَاءُكَ حَقٌّ وَقَوْلُكَ حَقٌّ وَالْجَنَّةُ حَقٌّ</Text>
                <Text style={{color:"#FFFFFF"}}> وَالنَّارُ حَقٌّ وَالنَّبِيُّوْنَ حَقٌّ وَمُحَمَّدٌ صَلَّى اللهُ عَلَيْهِ وَسَلَّمَ حَقٌّ وَالسَّاعَةُ حَقٌّ.</Text>
                <Text style={{color:"#FFFFFF"}}> اَللهُمَّ لَكَ اَسْلَمْتُ وَبِكَ اَمَنْتُ وَعَلَيْكَ تَوَكَّلْتُ وَاِلَيْكَ اَنَبْتُ وَبِكَ خَاصَمْتُ </Text>
                <Text style={{color:"#FFFFFF"}}>وَاِلَيْكَ حَاكَمْتُ فَاغْفِرْلِيْ مَاقَدَّمْتُ وَمَا اَخَّرْتُ وَمَا</Text>
                <Text style={{color:"#FFFFFF"}}>   اَسْرَرْتُ وَمَا اَعْلَنْتُ وَمَا اَنْتَ اَعْلَمُ بِهِ مِنِّيْ.</Text>
                <Text style={{color:"#FFFFFF"}}>اَنْتَ الْمُقَدِّمُ وَاَنْتَ الْمُؤَخِّرُ لاَاِلَهَ اِلاَّ اَنْتَ. وَلاَ حَوْلَ وَلاَ قُوَّةَ اِلاَّ بِاللهِ </Text>
 <Text style={{color:"red"}}>Catatan Sebelum Sholat Harus Tidur Dulu</Text>
        </View>
    )
}

export default Sholattahajjut;