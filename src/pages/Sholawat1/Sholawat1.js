import React from 'react';
import { Text, View,Image } from 'react-native';

const Sholawat1 = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4",alignContent:'center',alignItems:'center',marginTop:20 }} >
             <Text style={{color:"#FFFFFF"}} >أللّهُمَّ صَلِّ صَلَاةً كَامِلَةً وَسَلِّمْ سَلَامًا تَامًّا عَلَى سَيِّدِنَا مُحَمَّدِ الّذِي تَنْحَلُّ بِهِ</Text>
             <Text style={{color:"#FFFFFF"}} >الْعُقَدُ وَتَنْفَرِجُ بِهِ الْكُرَبُ وَتُقْضَى بِهِ الْحَوَائِجُ وَتُنَالُ بِهِ</Text>
             <Text style={{color:"#FFFFFF"}} >الرَّغَائِبُ وَحُسْنُ الْخَوَاتِمِ وَيُسْتَسْقَى الْغَمَامُ بِوَجْهِهِ</Text>
             <Text style={{color:"#FFFFFF"}} >الْكَرِيْمِ وَعَلَى آلِهِ وَصَحْبِهِ فِيْ كُلِّ لَمْحَةٍ وَنَفَسٍ بِعَدَدِ كُلِّ مَعْلُوْمٍ لَكَ</Text>
        </View>
    )
}

export default Sholawat1;