import React from 'react';
import { Text, View,Image } from 'react-native';

const Sholawat2 = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4",alignContent:'center',alignItems:'center',marginTop:20 }} >
             <Text style={{color:"#FFFFFF"}} >اللّٰهُمَّ صَلِّ عَلٰى سَيِّدِنَا </Text>
             <Text style={{color:"#FFFFFF"}} >    مُحَمَّدٍ طِبِّ الْقُلُوْبِ وَدَوَائِهَا </Text>
             <Text style={{color:"#FFFFFF"}} >    وَعَافِيَةِ الْاَبْدَانِ وَشِفَائِهَا </Text>
             <Text style={{color:"#FFFFFF"}} >    وَنُوْرِ الْاَبْصَارِ وَضِيَائِهَا </Text>
             <Text style={{color:"#FFFFFF"}} >      وَعَلٰى اٰلِهِ وَصَحْبِهِ وَسَلِّم  </Text>
              

        </View>
    )
}

export default Sholawat2;