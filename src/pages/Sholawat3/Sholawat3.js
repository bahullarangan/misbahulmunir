import React from 'react';
import { Text, View,Image } from 'react-native';

const Sholawat3 = () => {
    return(
        <View style={{flex: 1,backgroundColor:"#03a9f4",alignContent:'center',alignItems:'center',marginTop:20 }} >
             <Text style={{color:"#FFFFFF"}} >اللَّهُمَّ صَلِّ وَسَلِّمْ وَبَارِكْ عَلَى سَيِّدِنَا مُحَمَّدٍ الفَاتِحِ لِمَا أُغْلِقَ</Text>
             <Text style={{color:"#FFFFFF"}} >  وَالخَاتِمِ لِمَا سَبَقَ وَالنَّاصِرِ الحَقَّ بِالحَقِّ</Text>
             <Text style={{color:"#FFFFFF"}} >   وَالهَادِي اِلَى صِرَاطٍ مُسْتَقِيْمٍ.</Text>
             <Text style={{color:"#FFFFFF"}} >  صَلَّى اللهُ عَلَيْهِ وَعَلَى اَلِهِ وَأَصْحَابِهَ حَقَّ قَدْرِهِ وَمِقْدَارِهِ العَظِيْمِ</Text>

        </View>
    )
}

export default Sholawat3;