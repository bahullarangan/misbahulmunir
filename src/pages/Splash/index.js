import React, {useEffect} from 'react';
import { View, Text,Image,StatusBar} from 'react-native';


var Spinner = require ('react-native-spinkit');
const Splash = ({navigation}) => {
    useEffect(()=> {
        setTimeout(() => {
             navigation.replace('Daftar Isi')
        }, 5000)
    })
    return (
        <View style={{backgroundColor:'#03a9f4',flex:1,}}>
                 <StatusBar backgroundColor="#03a9f4" barStyle="light-content"/>
                

                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                <Image style={{width:200,height:140,alignContent:'center',marginBottom:50,marginLeft:30}} source={require('../Splash/kitab.png')}></Image>
                    <Text style={{fontSize:32, fontWeight:'bold', color:'#FFFFFF'}}>سلاح المطعين</Text>
                    <Text style={{fontSize: 18, color:'#FFFFFF'}}>Si Kecil Seribu Manfaat</Text>
                    <Spinner style={{marginTop:20}}size={50} type={'ThreeBounce'} color={'#FFFFFF'}/>
                </View>
                <Text style={{textAlign:'center',fontSize:18, color:'#FFFFFF'}}>Misbahul Munir</Text>
                <Text style={{textAlign:'center',marginBottom:20, color:'#FFFFFF'}}>Version 1.0 | 2021</Text>
            </View>
    );
};

export default Splash;