import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Splash, Home, Detail } from '../pages';


const Stack = createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator >
            <Stack.Screen name="Selamat Datang" component={Splash} options={{headerShown:false}} />  
            <Stack.Screen name="Daftar Isi" component={Home} options={{headerShown:false}}/> 
            <Stack.Screen name="Detail" component={Detail} />  
            
        </Stack.Navigator>
    )
}
export default Route